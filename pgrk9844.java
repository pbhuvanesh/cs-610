/* BHUVANESH ARASU cs610 9844 prp */
/********************************************
-----------------CS-610-----------------------
/////////Google Page Rank Algorithm//////////
Author: Bhuvanesh Arasu                     *
Desgined By: Larry Page                     *
Date: 11/05/2017                            *
Credits: Larry Page, Gerbessiotis & Anuradha*
Subject: CS-610                             *
Instructor:A. V. Gerbessiotis               *
Offering: Fall                              *
/////////Google Page Rank Algorithm//////////
-------------------CS-610--------------------
*********************************************/

/* Declaring Package Name for CS-610 Google Page Rank Algorithm */
//package com.bhuvanesh.googlepagerank;
/* Declaring Package Name for CS-610 Google Page Rank Algorithm */

/* Importing required Packages */
import java.util.*;
import java.nio.*;
import java.text.*;
import java.io.*;
import java.nio.file.*;
import static java.lang.Math.*;
/* Importing required Packages */

public class pgrk9844{
  private static List<ArrayList<Integer>> adjList;
  private static double[] pageRanks,
                          previousRanks,
                          currentPageRank;
  private static int[]    numberOfOutgoingEdges;
  private static int      numberOfNodes,
                          numberOfEdges,
                          inputIteration,
                          initialValue;
  private static double damping;
  private static double damping_Add;
  //---- Optional Colors ---//
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_BLACK = "\u001B[30m";
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_PURPLE = "\u001B[35m";
  public static final String ANSI_CYAN = "\u001B[36m";
  public static final String ANSI_WHITE = "\u001B[37m";
  public static final String[] colors = {
                                          "\u001B[34m",
                                          "\u001B[0m",
                                          "\u001B[31m",
                                          "\u001B[32m",
                                          "\u001B[33m",
                                          "\u001B[34m",
                                          "\u001B[35m",
                                          "\u001B[36m",
                                          "\u001B[37m"
                                        };
  //---- Optional Colors ---//

  public static void main(String ... args){
    List<String> inputLines;
    int counter = 0;
    initialValue = 0;
    boolean itr_value = false;
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
      System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666  11  00000000");
      System.out.println("CC            SS          66        11	00     0");
      System.out.println("CC            SS          66        11  0 0    0");
      System.out.println("CC            SSSSSSSSS   66666666  11	0  0   0");
      System.out.println("CC                   SS   66    66  11  0   0  0");
      System.out.println("CC                   SS   66    66  11  0    0 0");
      System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666 1111 00000000");
      System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }else{
      System.out.print(ANSI_BLUE);
      System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666  11  00000000");
      System.out.println("CC            SS          66        11	00     0");
      System.out.println("CC            SS          66        11  0 0    0");
      System.out.println("CC            SSSSSSSSS   66666666  11	0  0   0");
      System.out.println("CC                   SS   66    66  11  0   0  0");
      System.out.println("CC                   SS   66    66  11  0    0 0");
      System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666 1111 00000000");
      System.out.println(ANSI_YELLOW+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
    if(args.length < 3){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Please provide 3 arguments.Ex: java pgrk9844 <iterations> <rank> <input File>");
      }else{
        System.out.println(ANSI_RED+"ERROR: Please provide 3 arguments.Ex: java pgrk9844 <iterations> <rank> <input File>");
        System.out.println(ANSI_RESET);
      }
      return;
    }
    try{
      inputLines = readInputFile9844(args[2]);
    } catch(IOException io){
      System.out.println(io);
    }
    initialValue = Integer.parseInt(args[1]);
    if(numberOfNodes > 10){ inputIteration = 0; initialValue = -1; }
    initRank9844(initialValue);
    inputIteration = Integer.parseInt(args[0]);
    displayRank9844(counter);
    do{
      calculatePageRank9844();
      counter= counter + 1;
      if(numberOfNodes <= 10){ displayRank9844(counter); }
    }while(!stopIteration9844(counter));
    if(numberOfNodes > 10){ displayRankLargeNodes9844(counter); }
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
      System.out.println();
    }else{
      System.out.println(ANSI_RESET);
    }
  }

  public static List<String> readInputFile9844(String fileName) throws IOException{
      List<String> lines = Files.readAllLines(Paths.get(fileName));
      List<String> edgeNode = lines;
      int[] headerInfoIntArray = readHeadInformation9844(lines.get(0));
      setHeadInformation9844(headerInfoIntArray,lines.size());
      lines.remove(initialValue);
      initGraph9844();
      readNodeInformation9844(edgeNode);
      getNumberOfOutgoingEdges9844();
      return lines;
  }
  public static void initGraph9844(){
    adjList = new ArrayList<ArrayList<Integer>>(numberOfNodes);
    currentPageRank = new double[numberOfNodes];
    previousRanks = new double[numberOfNodes];
    numberOfOutgoingEdges = new int[numberOfNodes];;
    for (int i = 0; i < numberOfNodes; i++) {
			adjList.add(new ArrayList<>());
		}
  }
  public static void initRank9844(int initialValue){
    double initial = 0;
    damping = 0.85;
    damping_Add = (1-damping)/numberOfNodes;
    if(initialValue == 0 || initialValue == 1){
      initial = initialValue;
    }else if(initialValue == -1){
      initial = (double)1/numberOfNodes;
    }else if(initialValue == -2){
      initial = 1/Math.sqrt(numberOfNodes);
    }else{
      initial = initialValue;
    }
    for(int i = 0; i < numberOfNodes; i++){
      currentPageRank[i] = initial;
      previousRanks[i] = initial;
    }

  }
  public static int[] readHeadInformation9844(String headInformation){
    String[] headerInfoStrArray = headInformation.split(" ");
    int[] headerInfoIntArray = new int[headerInfoStrArray.length];
    headerInfoIntArray[0] = Integer.parseInt(headerInfoStrArray[0]);
    headerInfoIntArray[1] = Integer.parseInt(headerInfoStrArray[1]);
    return headerInfoIntArray;
  }

  public static void setHeadInformation9844(int[] headInfoIntArray,int edgesCounter){
    numberOfNodes = headInfoIntArray[0];
    numberOfEdges = headInfoIntArray[1];
    if(numberOfEdges < edgesCounter-1 ){
      System.out.println("Number of edges are lesser than given in the file");
      return;
    }else if(numberOfEdges > edgesCounter-1){
      System.out.println("Number of edges are greater than given in the file");
      return;
    }
  }

  public static void readNodeInformation9844(List<String> edgesInfo){
    String[] edges = new String[2];
    for(int i = 0; i < edgesInfo.size(); i++){
      edges = edgesInfo.get(i).split(" ");
      createAdjacencyList9844(edges);
    }

  }

  public static void createAdjacencyList9844(String[] edges){
    int[] edgeToFrom = new int[2];
    int fromNode,
        toNode;
    fromNode= Integer.parseInt(edges[0]);
    toNode = Integer.parseInt(edges[1]);
    //System.out.println(fromNode+"->"+toNode);
    if(fromNode == toNode){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Node["+fromNode+"] pointing to itself");
      }else{
        System.out.println(ANSI_RED+"ERROR: Node["+fromNode+"] pointing to itself");
        System.out.print(ANSI_RESET);
      }

      return;
    }
    if(toNode >= numberOfNodes){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Node["+toNode+"] does not exist");
      }else{
        System.out.println(ANSI_RED+"ERROR: Node["+toNode+"] does not exist"+ANSI_RESET);
      }

      return;
    }
    if(fromNode >= numberOfNodes){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Node["+fromNode+"] does not exist");
      }else{
        System.out.println(ANSI_RED+"ERROR: Node["+fromNode+"] does not exist"+ANSI_RESET);
      }

      return;
    }
    else{
    adjList.get(fromNode).add(toNode);
    }
  }

  public static void getNumberOfOutgoingEdges9844(){
    for(int i = 0; i < adjList.size(); i++){
      numberOfOutgoingEdges[i] = adjList.get(i).size();
      //System.out.println("Out:" + numberOfOutgoingEdges[i]);
    }
  }

  public static void calculatePageRank9844(){
    double temporaryValue = 0;
    double[] calculatedPageRank = new double[numberOfNodes];
    for (int i = 0; i < currentPageRank.length; i++) {
			for (int arylist = 0; arylist < adjList.size(); arylist++) {
				if (adjList.get(arylist).contains(i)) {
					temporaryValue = temporaryValue + (currentPageRank[arylist] / numberOfOutgoingEdges[arylist]);
				}
			}
			calculatedPageRank[i] = damping_Add + damping * temporaryValue;
      temporaryValue = 0;
		}
    previousRanks = currentPageRank;
    currentPageRank = calculatedPageRank;
  }
  public static boolean stopIteration9844(int currentIteration){
    double temp = 0;
		if (inputIteration > 0) {
			return currentIteration == inputIteration;
		}
		else {
			if (inputIteration == 0) {
				temp = 100000;
			}
			else  {
				temp = Math.pow(10, -inputIteration);
			}

			for (int i = 0; i < currentPageRank.length; i++) {
				if ((int)Math.floor(currentPageRank[i]*temp) != (int)Math.floor(previousRanks[i]*temp)) {
					return false;
				}
			}
			return true;
		}
  }

  public static void displayRank9844(int inputIter){
    int color = 1;
    DecimalFormat df = new DecimalFormat("0.0000000");
    if(inputIter == 0){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.print("Base : "+ inputIter + " : ");
      }else{
        System.out.print("Base : "+ inputIter + " : ");
      }
    }else{
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.print("Iterat : "+ inputIter + " : ");
      }else{
        System.out.print(ANSI_RESET+"Iterat : "+ inputIter + " : ");
      }

    }
    for (int i = 0; i < currentPageRank.length; i++) {
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
  			 System.out.print("PR["+ i + "] = "+df.format(currentPageRank[i]) + " ");
      }else{
        color ++;
        if(color == colors.length){ color = 1 ;}
  			 System.out.print(colors[color]+"PR["+ i + "] = "+df.format(currentPageRank[i]) + " ");
      }
		}
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
      System.out.println("");
    }else{
      System.out.println(ANSI_RESET);
    }

  }

  public static void displayRankLargeNodes9844(int inputIter){
    int color = 0;
    System.out.println("Iterat : "+ inputIter + " : ");
		DecimalFormat df = new DecimalFormat("0.0000000");
		for (int i = 0; i < currentPageRank.length; i++) {
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
  			System.out.println("PR["+ i + "] = "+df.format(currentPageRank[i]) + " ");
      }else{
        color ++;
        if(color == colors.length){ color = 0; }
  			System.out.println("PR["+ i + "] = "+df.format(currentPageRank[i]) + " ");
      }

		}
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
      System.out.println("");
    }else{
      System.out.println(ANSI_RESET);
    }
  }

}
