/* BHUVANESH ARASU cs610 9844 prp */
 /********************************************
 -----------------CS-610-----------------------
 /////////Google Page Rank Algorithm//////////
 Author: Bhuvanesh Arasu                     *
 Desgined By: Kleinberg                      *
 Date: 11/05/2017                            *
 Credits: Kleinberg , Gerbessiotis & Anuradha*
 Subject: CS-610                             *
 Instructor:A. V. Gerbessiotis               *
 Offering: Fall                              *
 /////////Kleinberg HITS Algorithm////////////
 -------------------CS-610--------------------
 *********************************************/


/* Importing required Packages */
import java.util.*;
import java.text.*;
import java.io.*;
import java.nio.file.*;
import static java.lang.Math.*;
/* Importing required Packages */

public class hits9844{
  private static List<ArrayList<Integer>> adjList;
  private static double[] authorities,
          previousAuthorities,
          previousHubs,
          hubs;
  //private static int[]    numberOfOutgoingEdges;
  private static int      numberOfNodes,
          numberOfEdges,
          inputIteration,
          initialValue;
 // private static double damping;
 // private static double damping_Add;
  //---- Optional Colors ---//
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_BLACK = "\u001B[30m";
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_PURPLE = "\u001B[35m";
  public static final String ANSI_CYAN = "\u001B[36m";
  public static final String ANSI_WHITE = "\u001B[37m";
  public static final String[] colors = {
          "\u001B[34m",
          "\u001B[0m",
          "\u001B[31m",
          "\u001B[32m",
          "\u001B[33m",
          "\u001B[34m",
          "\u001B[35m",
          "\u001B[36m",
          "\u001B[37m"
  };
  //---- Optional Colors ---//

  public static void main(String ... args){
    List<String> inputLines;
    int counter = 0;
    initialValue = 0;
    boolean itr_value = false;
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
     System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666  11  00000000");
     System.out.println("CC            SS          66        11	00     0");
     System.out.println("CC            SS          66        11  0 0    0");
     System.out.println("CC            SSSSSSSSS   66666666  11	0  0   0");
     System.out.println("CC                   SS   66    66  11  0   0  0");
     System.out.println("CC                   SS   66    66  11  0    0 0");
     System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666 1111 00000000");
     System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
   }else{
     System.out.print(ANSI_BLUE);
     System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666  11  00000000");
     System.out.println("CC            SS          66        11	00     0");
     System.out.println("CC            SS          66        11  0 0    0");
     System.out.println("CC            SSSSSSSSS   66666666  11	0  0   0");
     System.out.println("CC                   SS   66    66  11  0   0  0");
     System.out.println("CC                   SS   66    66  11  0    0 0");
     System.out.println("CCCCCCCCC     SSSSSSSSS	  66666666 1111 00000000");
     System.out.println(ANSI_YELLOW+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
   }
    if(args.length < 3){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
          System.out.println("ERROR: Please provide 3 arguments.Ex: java hits9844 <iterations> <rank> <input File>");
      }else{
        System.out.println(ANSI_RED+"ERROR: Please provide 3 arguments.Ex: java hits9844 <iterations> <rank> <input File>");
        System.out.println(ANSI_RESET);
      }

      return;
    }
    try{
      inputLines = readInputFile9844(args[2]);
    } catch(IOException io){
      System.out.println(io);
    }
    initialValue = Integer.parseInt(args[1]);
    if(numberOfNodes > 10){ inputIteration = 0; initialValue = -1; }
    initRank9844(initialValue);
    inputIteration = Integer.parseInt(args[0]);
    displayAuthoritiesAndHubs9844(counter);
    do{
      calculateAuthorities9844();
      calculateHubs9844();
      calculateSumOfSqrtsForAuthsAndHubs9844();
      ++counter;
      if(numberOfNodes <= 10){   displayAuthoritiesAndHubs9844(counter); }
    }while(!stopIteration9844(counter));
    if(numberOfNodes > 10){ displayRankLargeNodes9844(counter);
    }
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
      System.out.println();
    }else{
      System.out.println(ANSI_RESET);
    }
  }

  public static List<String> readInputFile9844(String fileName) throws IOException{
    List<String> lines = Files.readAllLines(Paths.get(fileName));
    List<String> edgeNode = lines;
    int[] headerInfoIntArray = readHeadInformation9844(lines.get(0));
    setHeadInformation9844(headerInfoIntArray,lines.size());
    lines.remove(initialValue);
    initGraph9844();
    readNodeInformation9844(edgeNode);
    return lines;
  }
  public static void initGraph9844(){
    adjList = new ArrayList<ArrayList<Integer>>(numberOfNodes);
    hubs = new double[numberOfNodes];
    authorities = new double[numberOfNodes];
    previousAuthorities = new double[numberOfNodes];
    previousHubs = new double[numberOfNodes];
    for (int i = 0; i < numberOfNodes; i++) {
      adjList.add(new ArrayList<>());
    }
  }
  public static void initRank9844(int initialValue){
    double initial = 0;
    //damping = 0.85;
    //damping_Add = (1-damping)/numberOfNodes;
    if(initialValue == 0 || initialValue == 1){
      initial = initialValue;
    }else if(initialValue == -1){
      initial = (double)1/numberOfNodes;
    }else if(initialValue == -2){
      initial = 1/Math.sqrt(numberOfNodes);
    }else{
      initial = initialValue;
    }
    for(int i = 0; i < numberOfNodes; i++){
      hubs[i] = initial;
      authorities[i] = initial;
    }

  }
  public static int[] readHeadInformation9844(String headInformation){
    String[] headerInfoStrArray = headInformation.split(" ");
    int[] headerInfoIntArray = new int[headerInfoStrArray.length];
    headerInfoIntArray[0] = Integer.parseInt(headerInfoStrArray[0]);
    headerInfoIntArray[1] = Integer.parseInt(headerInfoStrArray[1]);
    return headerInfoIntArray;
  }

  public static void setHeadInformation9844(int[] headInfoIntArray,int edgesCounter){
    numberOfNodes = headInfoIntArray[0];
    numberOfEdges = headInfoIntArray[1];
    if(numberOfEdges < edgesCounter-1 ){
      System.out.println("Number of edges are lesser than given in the file");
      System.exit(0);
      return;
    }else if(numberOfEdges > edgesCounter-1){
      System.out.println("Number of edges are greater than given in the file");
      System.exit(0);
      return;
    }
  }

  public static void readNodeInformation9844(List<String> edgesInfo){
    String[] edges = new String[2];
    for(int i = 0; i < edgesInfo.size(); i++){
      edges = edgesInfo.get(i).split(" ");
      createAdjacencyList9844(edges);
    }

  }

  public static void createAdjacencyList9844(String[] edges){
    int[] edgeToFrom = new int[2];
    int fromNode,
        toNode;
    fromNode= Integer.parseInt(edges[0]);
    toNode = Integer.parseInt(edges[1]);
    //System.out.println(fromNode+"->"+toNode);
    if(fromNode == toNode){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Node["+fromNode+"] pointing to itself");
      }else{
        System.out.println(ANSI_RED+"ERROR: Node["+fromNode+"] pointing to itself");
        System.out.print(ANSI_RESET);
      }

      return;
    }
    if(toNode >= numberOfNodes){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Node["+toNode+"] does not exist");
      }else{
        System.out.println(ANSI_RED+"ERROR: Node["+toNode+"] does not exist"+ANSI_RESET);
      }
      return;
    }
    if(fromNode >= numberOfNodes){
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        System.out.println("ERROR: Node["+fromNode+"] does not exist");
      }else{
        System.out.println(ANSI_RED+"ERROR: Node["+fromNode+"] does not exist"+ANSI_RESET);
      }
      return;
    }
    else{
      adjList.get(fromNode).add(toNode);
    }
  }

  public static void calculateAuthorities9844(){
    double temporaryValue = 0;
    for (int i = 0; i < authorities.length; i++) {
      previousAuthorities[i] = authorities[i];
      temporaryValue = 0;
      for (int j = 0; j < hubs.length; j++) {
        if (adjList.get(j).contains(i)) {
          temporaryValue = temporaryValue + hubs[j];
        }
      }
      authorities[i] = temporaryValue;
    }
  }

  public static void calculateHubs9844() {

    double temporaryValue = 0;
    for (int i = 0; i < hubs.length; i++) {
      previousHubs[i] = hubs[i];
      for (int j = 0; j < authorities.length; j++) {
        if (adjList.get(i).contains(j)) {
          temporaryValue = temporaryValue + authorities[j];
        }
      }
      hubs[i] = temporaryValue;
      temporaryValue = 0;
    }
  }

  public static boolean stopIteration9844(int currentIteration){
    double temp = 0;
    if (inputIteration > 0) {
      return currentIteration == inputIteration;
    }
    else {
      if (inputIteration == 0) {
        temp = 100000;
      }
      else  {
        temp = Math.pow(10, -inputIteration);
      }
      for (int i = 0; i < authorities.length; i++) {
        if ((int)Math.floor(authorities[i]*temp) != (int)Math.floor(previousAuthorities[i]*temp)) {
          return false;
        }
      }

      for (int i = 0; i < hubs.length; i++) {
        if ((int)Math.floor(hubs[i]*temp) != (int)Math.floor(previousHubs[i]*temp)) {
          return false;
        }
      }
      return true;
    }
  }

  public static void displayAuthoritiesAndHubs9844(int inputIter){
    int color = 0;
    DecimalFormat df = new DecimalFormat("0.0000000");
    if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
      if(inputIter == 0){
        System.out.print("Base : "+ inputIter + " : ");
      }else{
        System.out.print("Iterat : "+ inputIter + " : ");
      }
    }else{
      if(inputIter == 0){
        System.out.print(ANSI_RESET+"Base : "+ inputIter + " : ");
      }else{
        System.out.print(colors[0]+"Iterat : "+ inputIter + " : ");
      }
    }
    for (int i = 0; i < authorities.length; i++) {
      double authValue = 0,
             hubValue = 0;
      color ++;
      authValue = floor(authorities[i] * 10000000)/10000000;
      hubValue = floor(hubs[i] * 10000000)/10000000;
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        if(color == colors.length){ color = 0 ;}
        System.out.print("A/H["+ i + "] = "+df.format(authValue) + " / " + df.format(hubValue) + " ");
      }else{
        if(color == colors.length){ color = 0 ;}
        System.out.print(colors[color]+"A/H["+ i + "] = "+df.format(authValue) + " / " + df.format(hubValue) + " ");
        System.out.print(ANSI_RESET);
      }
    }
    System.out.println();

  }

  public static void displayRankLargeNodes9844(int inputIter){
    int color = 0;
    System.out.println("Iterat : "+ inputIter + " : ");
    DecimalFormat df = new DecimalFormat("0.0000000");
    for (int i = 0; i < hubs.length; i++) {
      color ++;
      if(System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").toLowerCase().contains("mac")){
        if(color == colors.length){ color = 0; }
        System.out.println("A/H["+ i + "] = "+df.format(Math.floor(authorities[i] * 1e7)/1e7) + " / " + df.format(Math.floor(hubs[i] * 1e7)/1e7) + " ");
        System.out.println();
      }else{
        if(color == colors.length){ color = 0; }
        System.out.println(colors[color]+"A/H["+ i + "] = "+df.format(Math.floor(authorities[i] * 1e7)/1e7) + " / " + df.format(Math.floor(hubs[i] * 1e7)/1e7) + " ");
        System.out.println(ANSI_RESET);
      }

    }
  }

  public static void sumOfSquaresOfVector9844(double[] vector){
    double sum = 0;
    for (int i = 0; i < vector.length; i++) {
      sum += pow(vector[i], 2);
    }
    sum = sqrt(sum);
    divideBySqrt9844(vector,sum);
  }

  public static void divideBySqrt9844(double[] vector, double sum){
    for (int i = 0; i < vector.length; i++) {
      vector[i] /= sum;
    }
  }

  public static void calculateSumOfSqrtsForAuthsAndHubs9844(){
    sumOfSquaresOfVector9844(authorities);
    sumOfSquaresOfVector9844(hubs);
  }

}
