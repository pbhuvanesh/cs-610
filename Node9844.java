/* BHUVANESH ARASU cs610 9844 prp */
public class Node9844
{
    private long frequency;
    private char ch;
    public boolean isInternalNode = false;
    private Node9844 leftChild;
    private Node9844 rightChild;


    public void setFrequency(long frequency){this.frequency = frequency;}
    public void setChar(char ch){this.ch = ch;}
    public void setLeftChild(Node9844 leftChild){this.leftChild = leftChild;}
    public void setRightChild(Node9844 rightChild){this.rightChild = rightChild;}


    public long getFrequency(){return this.frequency;}
    public char getChar(){return this.ch;}
    public Node9844 getLeftChild(){return this.leftChild;}
    public Node9844 getRightChild(){return this.rightChild;}
    public Node9844(long frequency, Node9844 leftChild, Node9844 rightChild)
    {
        if(!(leftChild == null && rightChild == null))
        {
            isInternalNode = true;
        }
        setFrequency(frequency);
        setLeftChild(leftChild);
        setRightChild(rightChild);
    }
    public Node9844(long frequency, char ch, Node9844 leftChild, Node9844 rightChild)
    {
        setFrequency(frequency);
        setChar(ch);
        setLeftChild(leftChild);
        setRightChild(rightChild);
    }

    public Node9844(long frequency, char ch)
    {
        setFrequency(frequency);
        setChar(ch);
        setLeftChild(null);
        setRightChild(null);
    }
    public void print()
    {
        System.out.println("Data : " + this.getChar() + "\tFrequency : " + this.getFrequency() + "\tLeftChild : " + this.getLeftChild().getChar() + "\tRightChild : " + this.getRightChild().getChar());
    }
}
